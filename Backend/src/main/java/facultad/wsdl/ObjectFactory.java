//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.05.27 a las 10:54:44 AM COT 
//


package facultad.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the facultad.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: facultad.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSedesRequest }
     * 
     */
    public GetSedesRequest createGetSedesRequest() {
        return new GetSedesRequest();
    }

    /**
     * Create an instance of {@link GetSedesResponse }
     * 
     */
    public GetSedesResponse createGetSedesResponse() {
        return new GetSedesResponse();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link GetJornadaRequest }
     * 
     */
    public GetJornadaRequest createGetJornadaRequest() {
        return new GetJornadaRequest();
    }

    /**
     * Create an instance of {@link GetJornadaResponse }
     * 
     */
    public GetJornadaResponse createGetJornadaResponse() {
        return new GetJornadaResponse();
    }

    /**
     * Create an instance of {@link GetProgramasRequest }
     * 
     */
    public GetProgramasRequest createGetProgramasRequest() {
        return new GetProgramasRequest();
    }

    /**
     * Create an instance of {@link GetProgramasResponse }
     * 
     */
    public GetProgramasResponse createGetProgramasResponse() {
        return new GetProgramasResponse();
    }

    /**
     * Create an instance of {@link GetCarrerasRequest }
     * 
     */
    public GetCarrerasRequest createGetCarrerasRequest() {
        return new GetCarrerasRequest();
    }

    /**
     * Create an instance of {@link GetCarrerasResponse }
     * 
     */
    public GetCarrerasResponse createGetCarrerasResponse() {
        return new GetCarrerasResponse();
    }

    /**
     * Create an instance of {@link GetAllFacultadesRequest }
     * 
     */
    public GetAllFacultadesRequest createGetAllFacultadesRequest() {
        return new GetAllFacultadesRequest();
    }

    /**
     * Create an instance of {@link GetAllFacultadesResponse }
     * 
     */
    public GetAllFacultadesResponse createGetAllFacultadesResponse() {
        return new GetAllFacultadesResponse();
    }

    /**
     * Create an instance of {@link GetGrupoMateriaRequest }
     * 
     */
    public GetGrupoMateriaRequest createGetGrupoMateriaRequest() {
        return new GetGrupoMateriaRequest();
    }

    /**
     * Create an instance of {@link GetGrupoMateriaResponse }
     * 
     */
    public GetGrupoMateriaResponse createGetGrupoMateriaResponse() {
        return new GetGrupoMateriaResponse();
    }

    /**
     * Create an instance of {@link GetFranjasRequest }
     * 
     */
    public GetFranjasRequest createGetFranjasRequest() {
        return new GetFranjasRequest();
    }

    /**
     * Create an instance of {@link GetFranjasResponse }
     * 
     */
    public GetFranjasResponse createGetFranjasResponse() {
        return new GetFranjasResponse();
    }

    /**
     * Create an instance of {@link GetMateriasxGrupoRequest }
     * 
     */
    public GetMateriasxGrupoRequest createGetMateriasxGrupoRequest() {
        return new GetMateriasxGrupoRequest();
    }

    /**
     * Create an instance of {@link GetMateriasxGrupoResponse }
     * 
     */
    public GetMateriasxGrupoResponse createGetMateriasxGrupoResponse() {
        return new GetMateriasxGrupoResponse();
    }

    /**
     * Create an instance of {@link GetGruposMateriasFranjProgRequest }
     * 
     */
    public GetGruposMateriasFranjProgRequest createGetGruposMateriasFranjProgRequest() {
        return new GetGruposMateriasFranjProgRequest();
    }

    /**
     * Create an instance of {@link GetGruposMateriasFranjProgResponse }
     * 
     */
    public GetGruposMateriasFranjProgResponse createGetGruposMateriasFranjProgResponse() {
        return new GetGruposMateriasFranjProgResponse();
    }

    /**
     * Create an instance of {@link GetAllMateriasRequest }
     * 
     */
    public GetAllMateriasRequest createGetAllMateriasRequest() {
        return new GetAllMateriasRequest();
    }

    /**
     * Create an instance of {@link GetAllMateriasResponse }
     * 
     */
    public GetAllMateriasResponse createGetAllMateriasResponse() {
        return new GetAllMateriasResponse();
    }

    /**
     * Create an instance of {@link GetAllActividadeBURequest }
     * 
     */
    public GetAllActividadeBURequest createGetAllActividadeBURequest() {
        return new GetAllActividadeBURequest();
    }

    /**
     * Create an instance of {@link GetAllActividadeBUResponse }
     * 
     */
    public GetAllActividadeBUResponse createGetAllActividadeBUResponse() {
        return new GetAllActividadeBUResponse();
    }

    /**
     * Create an instance of {@link GetAllCursoRequest }
     * 
     */
    public GetAllCursoRequest createGetAllCursoRequest() {
        return new GetAllCursoRequest();
    }

    /**
     * Create an instance of {@link GetAllCursoResponse }
     * 
     */
    public GetAllCursoResponse createGetAllCursoResponse() {
        return new GetAllCursoResponse();
    }

    /**
     * Create an instance of {@link GetMateriasxFacultadRequest }
     * 
     */
    public GetMateriasxFacultadRequest createGetMateriasxFacultadRequest() {
        return new GetMateriasxFacultadRequest();
    }

    /**
     * Create an instance of {@link GetMateriasxFacultadResponse }
     * 
     */
    public GetMateriasxFacultadResponse createGetMateriasxFacultadResponse() {
        return new GetMateriasxFacultadResponse();
    }

    /**
     * Create an instance of {@link GetMateriasxProgramaRequest }
     * 
     */
    public GetMateriasxProgramaRequest createGetMateriasxProgramaRequest() {
        return new GetMateriasxProgramaRequest();
    }

    /**
     * Create an instance of {@link GetMateriasxProgramaResponse }
     * 
     */
    public GetMateriasxProgramaResponse createGetMateriasxProgramaResponse() {
        return new GetMateriasxProgramaResponse();
    }

    /**
     * Create an instance of {@link GetMateriasxCodigoRequest }
     * 
     */
    public GetMateriasxCodigoRequest createGetMateriasxCodigoRequest() {
        return new GetMateriasxCodigoRequest();
    }

    /**
     * Create an instance of {@link GetMateriasxCodigoResponse }
     * 
     */
    public GetMateriasxCodigoResponse createGetMateriasxCodigoResponse() {
        return new GetMateriasxCodigoResponse();
    }

    /**
     * Create an instance of {@link Facultad }
     * 
     */
    public Facultad createFacultad() {
        return new Facultad();
    }

    /**
     * Create an instance of {@link Franja }
     * 
     */
    public Franja createFranja() {
        return new Franja();
    }

    /**
     * Create an instance of {@link Carrera }
     * 
     */
    public Carrera createCarrera() {
        return new Carrera();
    }

    /**
     * Create an instance of {@link Grupo }
     * 
     */
    public Grupo createGrupo() {
        return new Grupo();
    }

    /**
     * Create an instance of {@link Materia }
     * 
     */
    public Materia createMateria() {
        return new Materia();
    }

    /**
     * Create an instance of {@link Materiaprograma }
     * 
     */
    public Materiaprograma createMateriaprograma() {
        return new Materiaprograma();
    }

    /**
     * Create an instance of {@link Curso }
     * 
     */
    public Curso createCurso() {
        return new Curso();
    }

    /**
     * Create an instance of {@link Tutoria }
     * 
     */
    public Tutoria createTutoria() {
        return new Tutoria();
    }

    /**
     * Create an instance of {@link BienestarUniversitario }
     * 
     */
    public BienestarUniversitario createBienestarUniversitario() {
        return new BienestarUniversitario();
    }

    /**
     * Create an instance of {@link Sede }
     * 
     */
    public Sede createSede() {
        return new Sede();
    }

    /**
     * Create an instance of {@link Jornada }
     * 
     */
    public Jornada createJornada() {
        return new Jornada();
    }

    /**
     * Create an instance of {@link Programa }
     * 
     */
    public Programa createPrograma() {
        return new Programa();
    }

    /**
     * Create an instance of {@link GrupoMateria }
     * 
     */
    public GrupoMateria createGrupoMateria() {
        return new GrupoMateria();
    }

}
