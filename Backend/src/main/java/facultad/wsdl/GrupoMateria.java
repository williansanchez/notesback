//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.05.27 a las 10:54:44 AM COT 
//


package facultad.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para grupoMateria complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="grupoMateria"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cod_prog" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cod_grupo" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="franja_grupo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cant_matriculados" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="semestre" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="cod_materia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nombre_materia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "grupoMateria", propOrder = {
    "codProg",
    "codGrupo",
    "grupo",
    "franjaGrupo",
    "cantMatriculados",
    "semestre",
    "codMateria",
    "nombreMateria"
})
public class GrupoMateria {

    @XmlElement(name = "cod_prog", required = true)
    protected String codProg;
    @XmlElement(name = "cod_grupo")
    protected int codGrupo;
    @XmlElement(required = true)
    protected String grupo;
    @XmlElement(name = "franja_grupo", required = true)
    protected String franjaGrupo;
    @XmlElement(name = "cant_matriculados")
    protected int cantMatriculados;
    protected int semestre;
    @XmlElement(name = "cod_materia", required = true)
    protected String codMateria;
    @XmlElement(name = "nombre_materia", required = true)
    protected String nombreMateria;

    /**
     * Obtiene el valor de la propiedad codProg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProg() {
        return codProg;
    }

    /**
     * Define el valor de la propiedad codProg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProg(String value) {
        this.codProg = value;
    }

    /**
     * Obtiene el valor de la propiedad codGrupo.
     * 
     */
    public int getCodGrupo() {
        return codGrupo;
    }

    /**
     * Define el valor de la propiedad codGrupo.
     * 
     */
    public void setCodGrupo(int value) {
        this.codGrupo = value;
    }

    /**
     * Obtiene el valor de la propiedad grupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Define el valor de la propiedad grupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

    /**
     * Obtiene el valor de la propiedad franjaGrupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFranjaGrupo() {
        return franjaGrupo;
    }

    /**
     * Define el valor de la propiedad franjaGrupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFranjaGrupo(String value) {
        this.franjaGrupo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantMatriculados.
     * 
     */
    public int getCantMatriculados() {
        return cantMatriculados;
    }

    /**
     * Define el valor de la propiedad cantMatriculados.
     * 
     */
    public void setCantMatriculados(int value) {
        this.cantMatriculados = value;
    }

    /**
     * Obtiene el valor de la propiedad semestre.
     * 
     */
    public int getSemestre() {
        return semestre;
    }

    /**
     * Define el valor de la propiedad semestre.
     * 
     */
    public void setSemestre(int value) {
        this.semestre = value;
    }

    /**
     * Obtiene el valor de la propiedad codMateria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMateria() {
        return codMateria;
    }

    /**
     * Define el valor de la propiedad codMateria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMateria(String value) {
        this.codMateria = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreMateria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreMateria() {
        return nombreMateria;
    }

    /**
     * Define el valor de la propiedad nombreMateria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreMateria(String value) {
        this.nombreMateria = value;
    }

}
