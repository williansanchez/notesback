//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.05.27 a las 10:54:44 AM COT 
//


package facultad.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="facultad" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="codigosprogramas" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "facultad",
    "codigosprogramas"
})
@XmlRootElement(name = "GetMateriasxProgramaRequest")
public class GetMateriasxProgramaRequest {

    protected int facultad;
    @XmlElement(required = true)
    protected String codigosprogramas;

    /**
     * Obtiene el valor de la propiedad facultad.
     * 
     */
    public int getFacultad() {
        return facultad;
    }

    /**
     * Define el valor de la propiedad facultad.
     * 
     */
    public void setFacultad(int value) {
        this.facultad = value;
    }

    /**
     * Obtiene el valor de la propiedad codigosprogramas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigosprogramas() {
        return codigosprogramas;
    }

    /**
     * Define el valor de la propiedad codigosprogramas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigosprogramas(String value) {
        this.codigosprogramas = value;
    }

}
