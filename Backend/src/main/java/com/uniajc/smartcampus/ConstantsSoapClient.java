package com.uniajc.smartcampus;

public final class ConstantsSoapClient {
	
	public static final String HEADERSOURCE="<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"" + 
						"	xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" + 
		"<wsse:UsernameToken wsu:Id=\"UsernameToken-A2E995896AEE7431C0155321199835425\">" + 
			"<wsse:Username>SMARTMOBILITY_UNIAJC</wsse:Username>" + 
			"<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">SmartCampus2019*</wsse:Password>" + 
			"</wsse:UsernameToken>" +
			"</wsse:Security>";
	public static final String URI="http://smartdev.uniajc.edu.co:8888/wse_facultad_v1-0.0.1/ws";
	public static final String NAMESPACE="http://uniajc.edu.co/facultad/web-service/";

}