package com.uniajc.smartcampus.dto;

import com.uniajc.smartcampus.dto.util.LocationType;
import com.uniajc.smartcampus.dto.util.ResponseType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "SimpleObjectMessage - Object Response", description = "Clase para estandarizar los errores del servidor")
public class SimpleObjectMessage {

	@ApiModelProperty(notes = "Código de respuesta", required = true)
	private String codigo;
	@ApiModelProperty(notes = "Tipo de respuesta", required = true)
	private ResponseType tipo;
	@ApiModelProperty(notes = "Descripción de la respuesta", required = true)
	private LocationType descripcion;
	@ApiModelProperty(notes = "Ubicación donde se generó la respuesta", required = true)
	private String ubicacion;

	public SimpleObjectMessage() {
		super();
	}

	public SimpleObjectMessage(String codigo, ResponseType tipo, LocationType descripcion, String ubicacion) {
		super();
		this.codigo = codigo;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.ubicacion = ubicacion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public ResponseType getTipo() {
		return tipo;
	}

	public void setTipo(ResponseType tipo) {
		this.tipo = tipo;
	}

	public LocationType getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(LocationType descripcion) {
		this.descripcion = descripcion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

}
