package com.uniajc.smartcampus.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "SimpleObjectResponse - Object Response", description = "Clase para estandarizar las respuestas del servidor")
public class SimpleObjectResponse {

	@ApiModelProperty(notes = "Código de respuesta", required = true)
	private Integer codigo;
	@ApiModelProperty(notes = "Mensaje de respuesta", required = true)
	private String mensaje;
	@ApiModelProperty(notes = "Contenido de la respuesta", required = true)
	private Object valor;

	public SimpleObjectResponse() {
		super();
	}

	public SimpleObjectResponse(Integer codigo, String mensaje, Object valor) {
		super();
		this.codigo = codigo;
		this.mensaje = mensaje;
		this.valor = valor;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

}
