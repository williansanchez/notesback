package com.uniajc.smartcampus.models;

import java.time.Instant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Objeto - Model", description = "Entidad para administrar transacciones con ")
public class BaseObject {

	@ApiModelProperty(notes = "Identificación del objeto", required = true)
	private Long id;
	@ApiModelProperty(notes = "Fecha de creación")
	private Instant fechaCreacion;
	@ApiModelProperty(notes = "Descripción")
	private String descripcion;
	@ApiModelProperty(notes = "Estado")
	private Boolean estado;

	public BaseObject() {

	}

	public BaseObject(Long id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.estado = true;
		this.fechaCreacion = Instant.now();
	}
	
	public BaseObject(String descripcion) {
		super();
		this.descripcion = descripcion;
		this.estado = true;
		this.fechaCreacion = Instant.now();
	}

	public BaseObject(Long id, Instant fechaCreacion, String descripcion, Boolean estado) {
		super();
		this.id = id;
		this.fechaCreacion = fechaCreacion;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Instant fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}
