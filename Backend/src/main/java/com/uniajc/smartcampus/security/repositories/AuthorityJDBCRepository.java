package com.uniajc.smartcampus.security.repositories;

import java.util.List;

import com.uniajc.smartcampus.security.models.Authority;

public interface AuthorityJDBCRepository {
  
  public List<Authority> selectByLogin(String login);
  
}
