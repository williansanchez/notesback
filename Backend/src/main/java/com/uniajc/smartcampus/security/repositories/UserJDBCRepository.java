package com.uniajc.smartcampus.security.repositories;

import com.uniajc.smartcampus.security.models.JwtUser;

public interface UserJDBCRepository {

	public JwtUser selectByLogin(String login);
	
}
