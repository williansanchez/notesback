package com.uniajc.smartcampus.security.models;

/**
 *
 * @author Raul A. Hernandez
 */
public enum AuthorityType {
	ROLE_USER, ROLE_ADMIN
}