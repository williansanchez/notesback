package com.uniajc.smartcampus;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.xml.transform.StringSource;

import facultad.wsdl.GetProgramasRequest;
import facultad.wsdl.GetProgramasResponse;

public class SoapClient extends WebServiceGatewaySupport {

	public GetProgramasResponse getProgramasResponse(int codigo) {

		String action = ConstantsSoapClient.NAMESPACE + "GetProgramasRequest";

		GetProgramasRequest request = new GetProgramasRequest();
		request.setCodigoFacultad(codigo);


		MessageFactory msgFactory = null;
		try {
			msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		} catch (SOAPException e) {
			e.printStackTrace();
		}

		StringSource headerSource = new StringSource(ConstantsSoapClient.HEADERSOURCE);

		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplate = getWebServiceTemplate();
		wsTemplate.setMessageFactory(saajSoapMessageFactory);

		SoapActionCallback requestCallback = new SoapActionCallback(action) {
			public void doWithMessage(WebServiceMessage message) {
				SaajSoapMessage soapMessage = (SaajSoapMessage) message;
				SoapHeader soapHeader = soapMessage.getSoapHeader();

				Transformer transformer = null;
				try {
					transformer = TransformerFactory.newInstance().newTransformer();
					transformer.transform(headerSource, soapHeader.getResult());
				} catch (TransformerConfigurationException e1) {
					e1.printStackTrace();
				} catch (TransformerFactoryConfigurationError e1) {
					e1.printStackTrace();
				} catch (TransformerException e) {
					e.printStackTrace();
				}

				QName wsaToQName = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");
				SoapHeaderElement wsaTo = soapHeader.addHeaderElement(wsaToQName);
				wsaTo.setText(ConstantsSoapClient.URI);

				QName wsaActionQName = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
				SoapHeaderElement wsaAction = soapHeader.addHeaderElement(wsaActionQName);
				wsaAction.setText(action);
			}
		};

		GetProgramasResponse response = (GetProgramasResponse) wsTemplate.marshalSendAndReceive(ConstantsSoapClient.URI,
				request, requestCallback);

		return response;
	}

	
	
}
