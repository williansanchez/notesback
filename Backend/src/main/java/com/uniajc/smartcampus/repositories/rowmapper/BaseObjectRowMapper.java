package com.uniajc.smartcampus.repositories.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;

import com.uniajc.smartcampus.models.BaseObject;



public class BaseObjectRowMapper implements RowMapper<BaseObject> {

	@Override
	public BaseObject mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new BaseObject(rs.getLong("obj_id"), Timestamp.valueOf(rs.getString("obj_fecha_creacion")).toInstant(),
				rs.getString("obj_descripcion"), rs.getBoolean("obj_estado"));
	}

}
