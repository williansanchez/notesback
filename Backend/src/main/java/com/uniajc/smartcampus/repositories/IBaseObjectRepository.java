package com.uniajc.smartcampus.repositories;

import java.util.List;

import com.uniajc.smartcampus.models.BaseObject;

public interface IBaseObjectRepository extends IEntityRepository<BaseObject, Long> {

	public List<BaseObject> showByid(Long id);
	
}
