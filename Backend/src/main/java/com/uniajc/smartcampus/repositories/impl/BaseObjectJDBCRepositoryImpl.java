package com.uniajc.smartcampus.repositories.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

import com.uniajc.smartcampus.models.BaseObject;
import com.uniajc.smartcampus.repositories.IBaseObjectRepository;
import com.uniajc.smartcampus.repositories.rowmapper.BaseObjectRowMapper;
import com.uniajc.smartcampus.security.exceptions.BadRequestException;
import com.uniajc.smartcampus.security.exceptions.NotFoundException;

@Repository
public class BaseObjectJDBCRepositoryImpl implements IBaseObjectRepository {

	@Autowired
	@Qualifier("jdbcContratacion")
	private JdbcTemplate jdbc;

	@Override
	public void create(final BaseObject r) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO objeto VALUES(?, ?, ?, ?)");
			jdbc.execute(sql.toString(), new PreparedStatementCallback<Boolean>() {
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
					ps.setLong(1, r.getId());
					ps.setTimestamp(4, Timestamp.from(r.getFechaCreacion()),
							Calendar.getInstance(TimeZone.getTimeZone("America/Bogota")));
					ps.setString(3, r.getDescripcion());
					ps.setBoolean(2, r.getEstado());
					ps.execute();
					ps.close();
					return true;
				}
			});
		} catch (DuplicateKeyException e) {
			throw new BadRequestException("El registro con id=" + r.getId() + " ya existe");
		}
	}

	@Override
	public void update(Long id, BaseObject r) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE objeto SET obj_descripcion = ?, obj_estado = ? WHERE obj_id = ?");
		jdbc.update(sql.toString(), r.getDescripcion(), r.getEstado(), id);
	}

	@Override
	public void deleteWithUpdate(Long id) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE objeto " + "SET obj_estado = ? " + "WHERE obj_id = ?");
		jdbc.update(sql.toString(), false, id);
	}

	@Override
	public void delete(Long id) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM objeto WHERE obj_id = ?");
		jdbc.update(sql.toString(), id);
	}

	@Override
	public List<BaseObject> showAll() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM objeto");
		return jdbc.query(sql.toString(), new BaseObjectRowMapper());
	}

	@Override
	public BaseObject showById(Long id) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM objeto WHERE obj_id = ?");
			return jdbc.queryForObject(sql.toString(), new Object[] { id }, new BaseObjectRowMapper());
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException("No se encontró el registro con id=" + id);
		}
	}

	@Override
	public List<BaseObject> showByid(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
