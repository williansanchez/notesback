package com.uniajc.smartcampus.services.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uniajc.smartcampus.SoapClient;
import com.uniajc.smartcampus.dto.SimpleObjectResponse;
import com.uniajc.smartcampus.services.IBaseClientSoapService;

import facultad.wsdl.GetProgramasResponse;
import facultad.wsdl.Programa;

@Service
public class BaseClientSoapService implements IBaseClientSoapService {

	@Autowired
	private SoapClient soapClient;

	@Override
	public SimpleObjectResponse getAllProgramsByFaculty(int codigo) {
		GetProgramasResponse response = soapClient.getProgramasResponse(codigo);
		SimpleObjectResponse simple = new SimpleObjectResponse();
		simple.setCodigo(response.getRespuesta().getCodigo());

		try {
			Gson gson = new Gson();
			Type listType = new TypeToken<ArrayList<Programa>>() {
			}.getType();
			List<Programa> list = gson.fromJson(response.getRespuesta().getRespuesta(), listType);
			simple.setValor(list);
			simple.setMensaje("Listado obtenido con exito");

		} catch (Exception e) {

			simple.setMensaje(response.getRespuesta().getRespuesta());
			simple.setValor("");
		}

		return simple;
	}

}
