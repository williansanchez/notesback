package com.uniajc.smartcampus.services;

import com.uniajc.smartcampus.dto.SimpleObjectResponse;

public interface IBaseClientSoapService {
	
	SimpleObjectResponse getAllProgramsByFaculty(int codigo);
}
