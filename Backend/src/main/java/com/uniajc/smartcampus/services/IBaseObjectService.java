package com.uniajc.smartcampus.services;

import java.util.List;

import com.uniajc.smartcampus.models.BaseObject;

public interface IBaseObjectService extends IEntityService<BaseObject, Long>{

	public List<BaseObject> showByid(Long id);
	
}
