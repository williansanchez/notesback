package com.uniajc.smartcampus.services.impl;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniajc.smartcampus.models.BaseObject;
import com.uniajc.smartcampus.repositories.IBaseObjectRepository;
import com.uniajc.smartcampus.security.exceptions.BadRequestException;
import com.uniajc.smartcampus.security.exceptions.NotFoundException;
import com.uniajc.smartcampus.services.IBaseObjectService;
@Service
public class BaseObjectService implements IBaseObjectService {

	@Autowired
	private IBaseObjectRepository baseObjectRepository;

	@Override
	public void create(BaseObject r) throws BadRequestException {
		if (r == null) {
			throw new BadRequestException("La sanción no debe ser nula");
		}
		r.setEstado(true);
		r.setFechaCreacion(Instant.now());
		baseObjectRepository.create(r);
	}

	@Override
	public void update(Long id, BaseObject r) throws NotFoundException {
		if (r != null && id != null) {
			if (r.getId().equals(id)) {
				if (baseObjectRepository.showById(id) != null) {
					baseObjectRepository.update(id, r);
				}
			} else {
				throw new BadRequestException("Id y el id de la sanción deben coincidir");
			}
		} else {
			throw new BadRequestException("Id y sancion no pueden ser null");
		}
	}

	@Override
	public List<BaseObject> showAll() {
		return baseObjectRepository.showAll();
	}

	@Override
	public BaseObject showById(Long id) {
		if (id == null) {
			throw new BadRequestException("El id no puede ser null");
		}
		return baseObjectRepository.showById(id);
	}

	@Override
	public void delete(Long id) throws NotFoundException {
		if (baseObjectRepository.showById(id) != null) {
			baseObjectRepository.deleteWithUpdate(id);
		}
	}

	@Override
	public List<BaseObject> showByid(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
