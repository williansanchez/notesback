package com.uniajc.smartcampus.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.smartcampus.dto.SimpleObjectMessage;
import com.uniajc.smartcampus.dto.SimpleObjectResponse;
import com.uniajc.smartcampus.dto.util.LocationType;
import com.uniajc.smartcampus.dto.util.ResponseType;
import com.uniajc.smartcampus.models.BaseObject;
import com.uniajc.smartcampus.security.exceptions.BadRequestException;
import com.uniajc.smartcampus.security.exceptions.NotFoundException;
import com.uniajc.smartcampus.services.IBaseObjectService;

@RestController
@RequestMapping("/baseobject")
@PreAuthorize("hasRole('ESTUDIANTE')")
//@PreAuthorize("hasAnyRole('DECANO','DOCENTE','DIRECTOR_PROGRAMA','TRABAJADOR')")
@Api(tags = "Base Object Rest Controller", description = "Servicio ejemplo")
public class BaseObjectController {

	public static final String URL_CONTROLLER = "/baseobject";

	@Autowired
	private IBaseObjectService baseObjService;

	@ApiOperation(nickname = "create", notes = "Este método crear una objeto.", value = "Crear objeto", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "objeto creada con éxito"),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@PostMapping
	public ResponseEntity<SimpleObjectResponse> create(@RequestBody BaseObject dto) {
		try {
			baseObjService.create(dto);
			return new ResponseEntity<>(
					new SimpleObjectResponse(HttpStatus.OK.value(), "objeto creada con éxito", null), HttpStatus.OK);
		} catch (BadRequestException e) {
			return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.BAD_REQUEST.value(),
					"Error al insertar en la base de datos",
					new SimpleObjectMessage("ER-0002", ResponseType.ERROR, LocationType.OPERACION, URL_CONTROLLER)),
					HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(nickname = "update", notes = "Este método actualiza una objeto a partir de su Identificación", value = "Actualizar objeto a partir de su Identificación", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "objeto actualizada con éxito"),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@PutMapping("/{id}")
	public ResponseEntity<SimpleObjectResponse> update(@PathVariable Long id, @RequestBody BaseObject dto) {
		try {
			baseObjService.update(id, dto);
			return new ResponseEntity<>(
					new SimpleObjectResponse(HttpStatus.OK.value(), "objeto actualizada con éxito", null),
					HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.NOT_FOUND.value(), "El registro no existe",
					new SimpleObjectMessage("ER-0003", ResponseType.INFO, LocationType.FACHADA, URL_CONTROLLER)),
					HttpStatus.NOT_FOUND);
		} catch (BadRequestException e) {
			return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.BAD_REQUEST.value(),
					"Error al actualizar en la base de datos",
					new SimpleObjectMessage("ER-0002", ResponseType.ERROR, LocationType.OPERACION, URL_CONTROLLER)),
					HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(nickname = "delete", notes = "Este método elimina  _____ a partir de su Identificación", value = "Eliminar objeto a partir de su Identificación", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "objeto eliminada con éxito"),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@DeleteMapping("/{id}")
	public ResponseEntity<SimpleObjectResponse> delete(@PathVariable Long id) {
		baseObjService.delete(id);
		return new ResponseEntity<>(
				new SimpleObjectResponse(HttpStatus.OK.value(), "objeto eliminada con éxito", null), HttpStatus.OK);
	}

	@ApiOperation(nickname = "showAll", notes = "Este método obtiene todas _____ y las muestra en forma de lista.", value = "Ver todas las sanciones creadas", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sanciones obtenidas con éxito", response = BaseObject.class),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@GetMapping
	public ResponseEntity<SimpleObjectResponse> showAll() {
		return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.OK.value(), "Sanciones obtenidas con éxito",
				baseObjService.showAll()), HttpStatus.OK);
	}

	@ApiOperation(nickname = "showById", notes = "Este método obtiene _____ a partir de su identificación.", value = "Mostrar objeto a partir de su Identificación", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Snción obtenida con éxito", response = BaseObject.class),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@GetMapping("/{id}")
	public ResponseEntity<SimpleObjectResponse> showById(@PathVariable Long id) {
		try {
			return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.OK.value(), "objeto obtenida con éxito",
					baseObjService.showById(id)), HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(new SimpleObjectResponse(HttpStatus.NOT_FOUND.value(), "El registro no existe",
					new SimpleObjectMessage("ER-0003", ResponseType.INFO, LocationType.FACHADA, URL_CONTROLLER)),
					HttpStatus.NOT_FOUND);
		}
	}

}
