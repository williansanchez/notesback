package com.uniajc.smartcampus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.smartcampus.dto.SimpleObjectMessage;
import com.uniajc.smartcampus.dto.SimpleObjectResponse;
import com.uniajc.smartcampus.services.IBaseClientSoapService;

import facultad.wsdl.Facultad;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/soapClientFacultad")
@PreAuthorize("hasRole('ESTUDIANTE')")
//@PreAuthorize("hasAnyRole('DECANO','DOCENTE','DIRECTOR_PROGRAMA','TRABAJADOR')")
@Api(tags = "Base Object Rest Controller", description = "Servicio ejemplo")
public class BaseSoapClientController {
	
	public static final String URL_CONTROLLER = "/soapClientFacultad";
	
	@Autowired
	private IBaseClientSoapService baseClientSoapService;
	
	
	@ApiOperation(nickname = "showAllFacultad", notes = "Este método obtiene todas programas y las muestra en forma de lista.", value = "Ver todas las programas creadas", response = SimpleObjectResponse.class, produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "programas obtenidas con éxito", response = Facultad.class),
			@ApiResponse(code = 401, message = "Acceso no autorizado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 403, message = "Rol no permitido", response = SimpleObjectMessage.class),
			@ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = SimpleObjectMessage.class),
			@ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = SimpleObjectMessage.class) })
	@GetMapping("/showAllProgramasbyFaculty/{codigo}")
	public ResponseEntity<SimpleObjectResponse> showAllProgramasbyFaculty(@PathVariable int codigo) {
		return new ResponseEntity<>(baseClientSoapService.getAllProgramsByFaculty(codigo), HttpStatus.OK);
	}

}
