package com.uniajc.smartcampus.services;

import static org.junit.jupiter.api.Assertions.*;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.uniajc.smartcampus.models.BaseObject;
import com.uniajc.smartcampus.repositories.IEntityRepository;
import com.uniajc.smartcampus.security.exceptions.BadRequestException;
import com.uniajc.smartcampus.security.exceptions.NotFoundException;
import com.uniajc.smartcampus.services.IEntityService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class BaseObjectServiceTest {

	private static final Long ID = -1l;
	private static final String DESCRIPCION = "EJEMPLO";

	@Autowired
	private IEntityService<BaseObject, Long> service;
	
	@Autowired
	private IBaseClientSoapService base;

	@Autowired
	private IEntityRepository<BaseObject, Long> repository;

	@Before
	@DisplayName("inyectDependency")
	public void inyectionTest() {
		assertNotNull(service);
	}

	@Test
	@Order(1)
	@DisplayName("createSanction")
	public void createSanctionTest() {
		service.create(new BaseObject(ID, DESCRIPCION));
		assertNotNull(service.showById(ID));
	}

	@Test
	@Order(2)
	@DisplayName("createSanction_FAIL")
	public void createSanctionFailTest() {
		Assertions.assertThatExceptionOfType(BadRequestException.class).isThrownBy(() -> service.create(null));
	}

	@Test
	@Order(3)
	@DisplayName("updateSanction")
	public void BaseObject() {
		BaseObject temp = service.showById(ID);
		assertDoesNotThrow(() -> service.update(ID, temp));
	}
	
	@Test
	@Order(4)
	@DisplayName("updateSanction_FAIL")
	public void updateSanctionFailTest() {
		Assertions.assertThatExceptionOfType(BadRequestException.class).isThrownBy(() -> service.update(ID, null));
	}

	@Test
	@Order(5)
	@DisplayName("updateSanction_FAIL2")
	public void updateSanctionFail2Test() {
		BaseObject temp = new BaseObject(ID, DESCRIPCION);
		Assertions.assertThatExceptionOfType(BadRequestException.class).isThrownBy(() -> service.update(ID - 1L, temp));
	}

	@Test
	@Order(6)
	@DisplayName("updateSanction_FAIL3")
	public void updateSanctionFail3Test() {
		Assertions.assertThatExceptionOfType(BadRequestException.class).isThrownBy(() -> service.update(null, null));
	}

	@Test
	@Order(7)
	@DisplayName("updateSanction_FAIL4")
	public void updateSanctionFail4Test() {
		Assertions.assertThatExceptionOfType(BadRequestException.class)
				.isThrownBy(() -> service.update(null, new BaseObject()));
	}

	@Test
	@Order(8)
	@DisplayName("showAllSanctions")
	public void showAllSanctionsTest() {
		assertNotNull(service.showAll());
	}

	@Test
	@Order(9)
	@DisplayName("showSanctionById")
	public void showSanctionByIdTest() {
		assertNotNull(service.showById(ID));
	}

	@Test
	@Order(10)
	@DisplayName("showSanctionById_FAIL")
	public void showSanctionByIdFailTest() {
		Assertions.assertThatExceptionOfType(BadRequestException.class).isThrownBy(() -> service.showById(null));
	}

	@Test
	@Order(11)
	@DisplayName("showSanctionById_FAIL2")
	public void showSanctionByIdFail2Test() {
		Assertions.assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> service.showById(ID - 1L));
	}

	@Test
	@Order(12)
	@DisplayName("deleteSanction")
	public void deleteSanctionTest() {
		service.delete(ID);
		BaseObject temp = service.showById(ID);
		assertEquals(Boolean.FALSE, temp.getEstado());
		repository.delete(ID);
	}
	
	
	@Test
	@Order(13)
	@DisplayName("showAllProgrmas")
	public void showAllProgramassTest() {
		System.out.println(base.getAllProgramsByFaculty(115).getValor());
		assertNotNull(base.getAllProgramsByFaculty(115).getValor());
	}


}
