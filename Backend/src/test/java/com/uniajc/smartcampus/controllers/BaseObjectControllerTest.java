package com.uniajc.smartcampus.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.uniajc.smartcampus.controllers.BaseObjectController;
import com.uniajc.smartcampus.models.BaseObject;
import com.uniajc.smartcampus.repositories.IEntityRepository;
import com.uniajc.smartcampus.services.impl.BaseClientSoapService;

@TestMethodOrder(OrderAnnotation.class)
public class BaseObjectControllerTest extends AbstractTest {

	private static final Long ID = -1L;
	private static final String DESCRIPCION = "EJEMPLO";
	private static final String MODIFICACION = " EJEMPLO 2";

	@Autowired
	private IEntityRepository<BaseObject, Long> repository;

	@Test
	@DisplayName("createSanctionsTest")
	@Order(1)
	public void createSanctionsTest() throws Exception {
		BaseObject r = new BaseObject(ID, DESCRIPCION);
		mvc.perform(MockMvcRequestBuilders.post(BaseObjectController.URL_CONTROLLER).header(tokenHeader, TOKEN)
				.contentType(MediaType.APPLICATION_JSON).content(mapToJson(r)))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("createSanctionsTest_FAIL")
	@Order(2)
	public void createSanctionsFailTest() throws Exception {
		BaseObject r = new BaseObject(ID, DESCRIPCION);
		mvc.perform(MockMvcRequestBuilders.post(BaseObjectController.URL_CONTROLLER).header(tokenHeader, TOKEN)
				.contentType(MediaType.APPLICATION_JSON).content(mapToJson(r)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	@DisplayName("showAllSanctionsTest")
	@Order(3)
	public void showAllSanctionsTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(BaseObjectController.URL_CONTROLLER).header(tokenHeader, TOKEN))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("showSanctionsByIdTest")
	@Order(4)
	public void showSanctionsByIdTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(BaseObjectController.URL_CONTROLLER + "/" + ID).header(tokenHeader, TOKEN))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("showSanctionByIdTest_FAIL")
	@Order(5)
	public void showSanctionByIdFailTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(BaseObjectController.URL_CONTROLLER + "/" + (ID - 1L)).header(tokenHeader,
				TOKEN)).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	@DisplayName("updateSanctionTest")
	@Order(6)
	public void updateSanctionTest() throws Exception {
		BaseObject r = new BaseObject(ID, DESCRIPCION + MODIFICACION);
		mvc.perform(MockMvcRequestBuilders.put(BaseObjectController.URL_CONTROLLER + "/" + ID).header(tokenHeader, TOKEN)
				.contentType(MediaType.APPLICATION_JSON).content(mapToJson(r)))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@DisplayName("updateSanctionTest_FAIL")
	@Order(7)
	public void updateSanctionFailTest() throws Exception {
		BaseObject r = new BaseObject(ID - 1L, DESCRIPCION + MODIFICACION);
		mvc.perform(MockMvcRequestBuilders.put(BaseObjectController.URL_CONTROLLER + "/" + (ID))
				.header(tokenHeader, TOKEN).contentType(MediaType.APPLICATION_JSON).content(mapToJson(r)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	@DisplayName("updateSanctionTest_FAIL2")
	@Order(8)
	public void updateSanctionFail2Test() throws Exception {
		BaseObject r = new BaseObject(ID - 1L, DESCRIPCION + MODIFICACION);
		mvc.perform(MockMvcRequestBuilders.put(BaseObjectController.URL_CONTROLLER + "/" + (ID - 1L))
				.header(tokenHeader, TOKEN).contentType(MediaType.APPLICATION_JSON).content(mapToJson(r)))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	@DisplayName("deleteSanctionTest_FAIL")
	@Order(9)
	public void deleteSanctionFailTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.delete(BaseObjectController.URL_CONTROLLER + "/" + (ID - 1L))
				.header(tokenHeader, TOKEN)).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	@DisplayName("deleteSanctionTest")
	@Order(10)
	public void deleteSanctionTest() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.delete(BaseObjectController.URL_CONTROLLER + "/" + ID).header(tokenHeader, TOKEN))
				.andExpect(MockMvcResultMatchers.status().isOk());
		repository.delete(ID);
	}
	
}
